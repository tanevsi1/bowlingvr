# Bowling VR
This project was the first ever game I've made in Unity for VR as a school project. This repo is just for anyone who wants to try it out. 
Don't expect anything extra from it, it was just a way to learn Unity for me.

## Compatibility
To play, you need a SteamVR compatible headset and a Windows PC. Minimum requirements? I have no idea.
Tested on Win10, Ryzen 5 5600X, RTX 3070, 32GB RAM PC with Meta Quest 2 headset via QuestLink (listed specs are definitely overkill for this game, you could probably run this on a TI-84 graphical calculator).

## Controls
Standart SteamVR controls apply (on Meta Quest 2): Index Trigger: Hold Items; Joystick: Teleport Movement

## Support
Future Updates? No. Bug fixes? No (Have yet to find one though). Any updates in general? No, I'm done with this project and just wanted to share it.

## Game Launch
To launch the game, have SteamVR on and just open the "MVT Bowling.exe" executable file. To quit the game, just press the physical Quit button in-game.